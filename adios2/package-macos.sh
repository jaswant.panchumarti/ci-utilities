#!/bin/sh

# Run this script on a macOS host to generate adios2 binaries.

set -e
set -x

readonly target_arch="$( uname -m )"

case "$target_arch" in
    x86_64)
        min_macosx_version="10.13"
        target="macos-$target_arch"
        ;;
    arm64)
        min_macosx_version="11.0"
        target="macos-$target_arch"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly min_macosx_version
readonly target

# Keep in sync with the Windows script.
readonly git_url='https://github.com/ornladios/ADIOS2.git'
readonly git_tag='v2.10.1'

git clone "$git_url" adios2/src
git -C adios2/src -c advice.detachedHead=false checkout "$git_tag"

readonly macos_x86_64_target="10.13"
readonly macos_arm64_target="11.0"

readonly here="$( pwd )"

mkdir adios2/build
cd adios2/build
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_TESTING=OFF \
  -DADIOS2_BUILD_EXAMPLES=OFF \
  -DADIOS2_USE_Fortran=OFF \
  -DADIOS2_USE_Python=OFF \
  -DADIOS2_USE_MPI=OFF \
  -DCMAKE_OSX_DEPLOYMENT_TARGET="$min_macosx_version" \
  -DCMAKE_INSTALL_PREFIX="$here/install/adios2-$git_tag-$target" \
  "../src"
make "-j$( sysctl -n hw.ncpu )"
make "-j$( sysctl -n hw.ncpu )" install
cd ../../

# Create the final tarball containing the binaries.
tar czf "adios2-$git_tag-$target.tar.gz" -C "$here/install" "adios2-$git_tag-$target"
