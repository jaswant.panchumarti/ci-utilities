param (
  [switch] $UseMPI
)

$erroractionpreference = "stop"

$use_mpi="OFF"
$mpi_tag="nompi"
if ($UseMPI -eq $true)
{
  $use_mpi="ON"
  $mpi_tag="mpi"
}

# Keep in sync with the MacOS script.
$git_url = 'https://github.com/ornladios/ADIOS2.git'
$git_tag = 'v2.10.1'

git clone "$git_url" adios2/src
git -C adios2/src -c advice.detachedHead=false checkout "$git_tag"

$here = pwd

New-Item -Path "adios2\build" -Type Directory
cd "adios2\build"
cmake `
  -GNinja `
  -DCMAKE_BUILD_TYPE=Release `
  -DBUILD_TESTING=OFF `
  -DADIOS2_BUILD_EXAMPLES=OFF `
  -DADIOS2_USE_Fortran=OFF `
  -DADIOS2_USE_Python=OFF `
  -DADIOS2_USE_MPI:BOOL=$use_mpi `
  -DADIOS2_USE_SST=OFF `
  -DCMAKE_INSTALL_PREFIX="$here\install\adios2-$mpi_tag-$git_tag-windows-x86_64" `
  "..\src"
ninja
ninja install
cd ..\..

# create final zip
Compress-Archive -LiteralPath "$here\install\adios2-$mpi_tag-$git_tag-windows-x86_64" -DestinationPath "$here\adios2-$mpi_tag-$git_tag-windows-x86_64.zip"
