#!/bin/bash

set -e

readonly git_url='https://gitlab.kitware.com/utils/webhook-listen.git'
readonly git_commit='fa8cd268de4daa13909d847e011b6d4d62e46cbd' # clap fixes

git clone "$git_url" webhook-listen/src
pushd webhook-listen/src
git -c advice.detachedHead=false checkout "$git_commit"
short_commit="$( git rev-parse --short "$git_commit" )"
readonly short_commit
cargo build --features systemd
popd
mv webhook-listen/src/target/debug/webhook-listen "webhook-listen-$short_commit"
