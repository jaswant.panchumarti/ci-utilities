#!/bin/sh

set -e
set -x

readonly target_arch="$( uname -m )"

case "$target_arch" in
    x86_64)
        min_macosx_version="10.13"
        target="macos-$target_arch"
        ;;
    arm64)
        min_macosx_version="11.0"
        target="macos-$target_arch"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly min_macosx_version
readonly target

readonly bison_version="3.8.2"
readonly bison_filename="bison-$bison_version"
readonly bison_tarball="$bison_filename.tar.xz"
readonly bison_url="http://mirror.rit.edu/gnu/bison/$bison_tarball"
readonly bison_sha256sum="9bba0214ccf7f1079c5d59210045227bcf619519840ebfa80cd3849cff5a5bf2"

echo "$bison_sha256sum  $bison_tarball" > bison.sha256sum
curl -OL "$bison_url"
shasum -a 256 --check bison.sha256sum
tar xf "$bison_tarball"

export MACOSX_DEPLOYMENT_TARGET="$min_macosx_version"

cd "$bison_filename"
./configure \
    --enable-relocatable \
    --prefix="$PWD/bison-$bison_version-$target"
make "-j$( sysctl -n hw.ncpu )"
make "-j$( sysctl -n hw.ncpu )" install

tar -cJf "../bison-$bison_version-$target.tar.xz" "bison-$bison_version-$target"
