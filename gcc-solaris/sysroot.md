# Generating Sysroot Tarballs

Generate a list of relevant paths to extract from a Solaris host:

```console
$ ./sysroot-paths.bash > sysroot-paths.lst
```

The list has already been recorded in an adjacent file.

Copy the relevant paths from Solaris hosts:

```console
$ ./sysroot-rsync.bash SOME-sunos5.10-i386-HOST:/  i386-pc-solaris2.10   --rsync-path=/opt/csw/bin/rsync
$ ./sysroot-rsync.bash SOME-sunos5.10-sparc-HOST:/ sparc-sun-solaris2.10 --rsync-path=/opt/csw/bin/rsync
```

Create the tarballs:

```console
$ tar cJf sysroot-i386-pc-solaris2.10-sunos5.10-1.tar.xz   sysroot/i386-pc-solaris2.10/*
$ tar cJf sysroot-sparc-sun-solaris2.10-sunos5.10-1.tar.xz sysroot/sparc-sun-solaris2.10/*
```
