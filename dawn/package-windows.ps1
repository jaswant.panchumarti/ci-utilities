$ErrorActionPreference = "Stop"

# Keep in sync with the unix script.
$git_url = "https://dawn.googlesource.com/dawn"
# Dawn doesn't have any version tags. Instead the tags refer to chromium's patch version.
$chromium_patch_version = "7033"
$git_tag = "chromium/$chromium_patch_version"

git clone --depth=1 --branch "$git_tag" "$git_url" dawn/src

$here = pwd

cmake `
  -GNinja `
  -C "dawn\dawn.cmake" `
  -S "dawn\src" `
  -B "dawn\build"
cmake --build "dawn\build"
cmake --install "dawn\build" --prefix "$here\install\dawn-$chromium_patch_version-windows-x86_64"

# create final zip
Compress-Archive `
  -LiteralPath "$here\install\dawn-$chromium_patch_version-windows-x86_64" `
  -DestinationPath "$here\dawn-$chromium_patch_version-windows-x86_64.zip"
