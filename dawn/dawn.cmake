set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")

##############
# Dawn options
##############
set(DAWN_BUILD_SAMPLES OFF CACHE BOOL "")
set(DAWN_ENABLE_DESKTOP_GL OFF CACHE BOOL "") # Don't need OpenGL on any platform.
set(DAWN_ENABLE_INSTALL ON CACHE BOOL "")
set(DAWN_ENABLE_OPENGLES OFF CACHE BOOL "") # Don't need OpenGL ES on any platform.
set(DAWN_ENABLE_NULL OFF CACHE BOOL "")
if (WIN32)
    set(DAWN_ENABLE_VULKAN OFF CACHE BOOL "") # Disable vulkan on windows in favor of D3D11 and D3D12
    set(DAWN_ENABLE_SPIRV_VALIDATION OFF CACHE BOOL "")
    set(DAWN_USE_BUILT_DXC ON CACHE BOOL "") # Builds DXC from scratch because we cannot rely upon windows machine having dxc headers.
endif ()
set(DAWN_FETCH_DEPENDENCIES ON CACHE BOOL "")
set(DAWN_USE_GLFW OFF CACHE BOOL "")

##############
# Tint options
##############
set(TINT_BUILD_CMD_TOOLS OFF CACHE BOOL "")
set(TINT_BUILD_GLSL_VALIDATOR OFF CACHE BOOL "") # not needed because OpenGL is disabled
set(TINT_BUILD_GLSL_WRITER OFF CACHE BOOL "") # not needed because OpenGL is disabled
set(TINT_BUILD_IR_BINARY OFF CACHE BOOL "")
set(TINT_BUILD_TESTS OFF CACHE BOOL "")
