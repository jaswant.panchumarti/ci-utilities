if (NOT TARGET emdawnwebgpu)
  add_library(emdawnwebgpu STATIC IMPORTED GLOBAL)
endif ()

# Get the installation prefix
get_filename_component(emdawnwebgpu_install_dir "${CMAKE_CURRENT_LIST_FILE}" DIRECTORY )
set(emdawnwebgpu_install_dir "${emdawnwebgpu_install_dir}/../../..")

# Set include directories
set(_emdawnwebgpu_include_dir "${emdawnwebgpu_install_dir}/include")

# Set library dir
set(_emdawnwebgpu_lib_dir "${emdawnwebgpu_install_dir}/lib")

set(emdawnwebgpu_link_options
  # We are using Dawn-generated bindings, not those that emscripten provides
  "-sUSE_WEBGPU=0"
  # The JS libraries needed for bindings
  "--js-library=${_emdawnwebgpu_lib_dir}/library_webgpu_enum_tables.js"
  "--js-library=${_emdawnwebgpu_lib_dir}/library_webgpu_generated_struct_info.js"
  "--js-library=${_emdawnwebgpu_lib_dir}/library_webgpu_generated_sig_info.js"
  "--js-library=${_emdawnwebgpu_lib_dir}/library_webgpu.js"
  "--closure-args=--externs=${_emdawnwebgpu_lib_dir}/webgpu-externs.js")

set_target_properties(emdawnwebgpu PROPERTIES
  INTERFACE_LINK_OPTIONS "${emdawnwebgpu_link_options}"
  INTERFACE_INCLUDE_DIRECTORIES "${_emdawnwebgpu_include_dir}"
  IMPORTED_LOCATION "${_emdawnwebgpu_lib_dir}/libemdawnwebgpu_c.a"
)
