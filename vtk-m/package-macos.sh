#!/bin/sh

# Run this script on a macOS host to generate vtk-m binaries.

set -e
set -x

readonly target_arch="$( uname -m )"

case "$target_arch" in
    x86_64)
        min_macosx_version="10.13"
        target="macos-$target_arch"
        ;;
    arm64)
        min_macosx_version="11.0"
        target="macos-$target_arch"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly min_macosx_version
readonly target

# Keep in sync with the Windows script.
readonly git_url='https://gitlab.kitware.com/vtk/vtk-m.git'
readonly git_tag='v2.1.0'

git clone "$git_url" vtkm/src
git -C vtkm/src -c advice.detachedHead=false checkout "$git_tag"

readonly macos_x86_64_target="10.13"
readonly macos_arm64_target="11.0"

readonly here="$( pwd )"

mkdir vtkm/build
cd vtkm/build
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_SHARED_LIBS=ON \
  -DBUILD_TESTING=OFF \
  -DVTKm_ENABLE_TESTING=OFF \
  -DCMAKE_OSX_DEPLOYMENT_TARGET="$min_macosx_version" \
  -DCMAKE_INSTALL_PREFIX="$here/install/vtkm-$git_tag-$target" \
  "../src"
make "-j$( sysctl -n hw.ncpu )"
make "-j$( sysctl -n hw.ncpu )" install
cd ../../

# Create the final tarball containing the binaries.
tar czf "vtkm-$git_tag-$target.tar.gz" -C "$here/install" "vtkm-$git_tag-$target"
