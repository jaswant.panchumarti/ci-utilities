#!/bin/sh

set -e

readonly version="3.8.2"
readonly date="20241209.1"

case "$( uname -s )-$( uname -m )" in
    Darwin-arm64)
        shatool="shasum -a 256"
        sha256sum="075cd9fff2288d572f6749240c8b0c6d0f81f411e851aaa449473f3bacceda55"
        platform="macos-arm64"
        ;;
    Darwin-x86_64)
        shatool="shasum -a 256"
        sha256sum="af41ee6150ebc8363b23e84d8bc5bb21767b50f69d772514400ff3b94770344a"
        platform="macos-x86_64"
        ;;
    *)
        echo "Unrecognized platform $( uname -s )"
        exit 1
        ;;
esac
readonly shatool
readonly sha256sum
readonly platform

readonly filename="bison-$version-$platform"
readonly tarball="$filename.tar.xz"

cd .gitlab

echo "$sha256sum  $tarball" > bison.sha256sum
curl -OL "https://gitlab.kitware.com/api/v4/projects/6955/packages/generic/bison/v$version-$date/$tarball"
$shatool --check bison.sha256sum
tar xf "$tarball"
mv "$filename" bison
